package com.sample.mylibrary;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class LibraryTest {

  @Test
  public void testHello() {
    Library lib = new Library();
    System.out.println(lib.hello("world"));
    assertEquals("Hello, world!", lib.hello("world"));
  }
}
