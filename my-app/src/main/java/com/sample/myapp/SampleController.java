package com.sample.myapp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.sample.mylibrary.Library;

@RestController
public class SampleController {

  @GetMapping("/")
  public String hello(@RequestParam(value = "v", defaultValue = "World") String value) {
    return new Library().hello(value);
  }
}